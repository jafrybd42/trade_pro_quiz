<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Questions extends Model
{
    use HasFactory;

    public function questionSet()
    {
    	return $this->belongsTo(QuestionSet::class, 'question_set_id');
    }


}
