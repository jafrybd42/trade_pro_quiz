<?php

namespace App\Http\Controllers;


use App\Models\QuestionSet;
use App\Http\Controllers\CommonController;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $questionSets = QuestionSet::where([['status', '=', 1], ['cat_id', '=', 0]])->get();

        // dd($questionSets);
        for ($i=0; $i < count($questionSets) ; $i++) { 
            $questionSubCategoryList = QuestionSet::where([['cat_id', '=', $questionSets[$i]->id], ['status', '=', 1]])->get();

            if(count($questionSubCategoryList) > 0){
                $questionSets[$i]['willShowTOExam'] = true;
            } else if (count($questionSets[$i]->questions) > 0) {
                $questionSets[$i]['willShowTOExam'] = true;
            } else {
                $questionSets[$i]['willShowTOExam'] = false;
            }
        }

        return view('index', compact('questionSets'));
    }
}
