<?php

namespace App\Http\Controllers;

use App\Models\Questions;
use App\Models\QuestionSet;
use App\Http\Controllers\CommonController;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class QuestionsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->common_class_obj = new CommonController();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }


    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validatedata = $request->validate([
            
            'correct_answer' => 'required|numeric|min:1'
        ]);

       
        $question_set_id = $request->question_set_id;

        if(($request->question_text == null ) && ($request->question_image == null)){
            return redirect()->route('questions.createQuestion',compact('question_set_id'))->with('error', 'Question should have text or image');
        }

        if(($request->option_1_text == null) && ($request->option_1_image == null)){
            return redirect()->route('questions.createQuestion',compact('question_set_id'))->with('error', 'Option 1 should have text or image');
        }

        if(($request->option_2_text == null ) && ($request->option_2_image == null )){
            return redirect()->route('questions.createQuestion',compact('question_set_id'))->with('error', 'Option 2 should have text or image');
        }

        if(($request->option_3_text == null ) && ($request->option_3_image == null )){
            return redirect()->route('questions.createQuestion',compact('question_set_id'))->with('error', 'Option 3 should have text or image');
        }

        if(($request->option_4_text == null ) && ($request->option_4_image == null )){
            return redirect()->route('questions.createQuestion',compact('question_set_id'))->with('error', 'Option 4 should have text or image');
        }

        if(($request->correct_answer <= 0) || ($request->correct_answer > 4)){
            return redirect()->route('questions.createQuestion',compact('question_set_id'))->with('error', 'Correct Answer should be between 1 to 4');
        }


        $question = new Questions();
        $question->question_set_id  = $question_set_id;

        $question->question_text = $request->question_text;
        
        if ($request->hasFile('question_image')) { // if image is not empty.

            $request->validate([
                'question_image' => 'required|image|mimes:jpeg,png,jpg|max:2048',
            ]);

            $slug = Str::slug($request->question_image);

            $currentDate = Carbon::now()->toDateString();
            $imageQuestion = $slug . '-' . $currentDate . '-' . uniqid() . '.' .$request->question_image->getClientOriginalExtension();

            if (!file_exists('images/questions')) {
                mkdir('images/questions', 0777, true);
            }
            $request->question_image->move('images/questions', $imageQuestion);
        
            $question->question_image = $imageQuestion;
        
        }

        

        $question->option_1_text = $request->option_1_text;
        if ($request->hasFile('option_1_image')) { // if image is not empty.
            
            $request->validate([
                'option_1_image' => 'required|image|mimes:jpeg,png,jpg|max:2048',
            ]);

            $slug = Str::slug($request->option_1_image);

            $currentDate = Carbon::now()->toDateString();
            $imageName1 = $slug . '-' . $currentDate . '-' . uniqid() . '.' .$request->option_1_image->getClientOriginalExtension();

            if (!file_exists('images/questions')) {
                mkdir('images/questions', 0777, true);
            }
            $request->option_1_image->move('images/questions', $imageName1);

            $question->option_1_image = $imageName1;
        }
        

        $question->option_2_text = $request->option_2_text;
        if ($request->hasFile('option_2_image')) { // if image is not empty.
            
            $request->validate([
                'option_2_image' => 'required|image|mimes:jpeg,png,jpg|max:2048',
            ]);

            $slug = Str::slug($request->option_2_image);

            $currentDate = Carbon::now()->toDateString();
            $imageName2 = $slug . '-' . $currentDate . '-' . uniqid() . '.' .$request->option_2_image->getClientOriginalExtension();

            if (!file_exists('images/questions')) {
                mkdir('images/questions', 0777, true);
            }
            $request->option_2_image->move('images/questions', $imageName2);

            $question->option_2_image = $imageName2;
        }
        

        $question->option_3_text = $request->option_3_text;
        if ($request->hasFile('option_3_image')) { // if image is not empty.
           
            $request->validate([
                'option_3_image' => 'required|image|mimes:jpeg,png,jpg|max:2048',
            ]);

            $slug = Str::slug($request->option_3_image);

            $currentDate = Carbon::now()->toDateString();
            $imageName3 = $slug . '-' . $currentDate . '-' . uniqid() . '.' .$request->option_3_image->getClientOriginalExtension();

            if (!file_exists('images/questions')) {
                mkdir('images/questions', 0777, true);
            }
            $request->option_3_image->move('images/questions', $imageName3);

            $question->option_3_image = $imageName3;
            
        }

        $question->option_4_text = $request->option_4_text;
        if ($request->hasFile('option_4_image')) { // if image is not empty.
           
            $request->validate([
                'option_4_image' => 'required|image|mimes:jpeg,png,jpg|max:2048',
            ]);

            $slug = Str::slug($request->option_4_image);

            $currentDate = Carbon::now()->toDateString();
            $imageName4 = $slug . '-' . $currentDate . '-' . uniqid() . '.' .$request->option_4_image->getClientOriginalExtension();

            if (!file_exists('images/questions')) {
                mkdir('images/questions', 0777, true);
            }
            $request->option_4_image->move('images/questions', $imageName4);

            $question->option_4_image = $imageName4;
        }

        $question->correct_answer = $request->correct_answer;

        $question->created_by = Auth::user()->id;
        $question->updated_by = Auth::user()->id;


        $question->save();

        if ($question) {
            return redirect()->route('questions.listOfQuestions',compact('question_set_id'))->with('success', 'Question  Has Been Added Successfully');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Questions  $questions
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $question = Questions::findorFail($id);

        return view('questions.details', compact('question','id'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Questions  $questions
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
       
        $question = Questions::findorFail($id);

        return view('questions.edit', compact('question','id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Questions  $questions
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        //
        $validatedata = $request->validate([
            
            'correct_answer' => 'required|numeric|min:1'
        ]);

        $question = Questions::findorFail($id);
        $existing_question_image = $question->question_image;
        $existing_option_1_image = $question->option_1_image;
        $existing_option_2_image = $question->option_2_image;
        $existing_option_3_image = $question->option_3_image;
        $existing_option_4_image = $question->option_4_image;


        $question_set_id = $request->question_set_id;


        if(($request->question_text == null ) && ($request->question_image == null ) && ($question->question_image == null)){
            return redirect()->route('questions.listOfQuestions',compact('question_set_id'))->with('error', 'Question should have text or image');
        }

        if(($request->option_1_text == null ) && ($request->option_1_image == null )  && ($question->option_1_image == null)){
            return redirect()->route('questions.listOfQuestions',compact('question_set_id'))->with('error', 'Option 1 should have text or image');
        }

        if(($request->option_2_text == null ) && ($request->option_2_image == null )  && ($question->option_2_image == null)){
            return redirect()->route('questions.listOfQuestions',compact('question_set_id'))->with('error', 'Option 2 should have text or image');
        }

        if(($request->option_3_text == null ) && ($request->option_3_image == null )  && ($question->option_3_image == null)){
            return redirect()->route('questions.listOfQuestions',compact('question_set_id'))->with('error', 'Option 3 should have text or image');
        }

        if(($request->option_4_text == null ) && ($request->option_4_image == null )  && ($question->option_4_image == null)){
            return redirect()->route('questions.listOfQuestions',compact('question_set_id'))->with('error', 'Option 4 should have text or image');
        }

        if(($request->correct_answer <= 0) || ($request->correct_answer > 4)){
            return redirect()->route('questions.listOfQuestions',compact('question_set_id'))->with('error', 'Correct Answer should be between 1 to 4');
        }



        $question->question_text = $request->question_text;
        
        if ($request->hasFile('question_image')) { // if image is not empty.

            $request->validate([
                'question_image' => 'required|image|mimes:jpeg,png,jpg|max:2048',
            ]);

            $slug = Str::slug($request->question_image);

            $currentDate = Carbon::now()->toDateString();
            $imageQuestion = $slug . '-' . $currentDate . '-' . uniqid() . '.' .$request->question_image->getClientOriginalExtension();

            if (!file_exists('images/questions')) {
                mkdir('images/questions', 0777, true);
            }
            $request->question_image->move('images/questions', $imageQuestion);

            /// Delete Existing Image
            if($existing_question_image != null){
                if (file_exists('images/questions/' . $existing_question_image)) {
                    unlink('images/questions/' . $existing_question_image);
                }
            }
        
            $question->question_image = $imageQuestion;
        
        }

        

        $question->option_1_text = $request->option_1_text;
        if ($request->hasFile('option_1_image')) { // if image is not empty.
            
            $request->validate([
                'option_1_image' => 'required|image|mimes:jpeg,png,jpg|max:2048',
            ]);

            $slug = Str::slug($request->option_1_image);

            $currentDate = Carbon::now()->toDateString();
            $imageName1 = $slug . '-' . $currentDate . '-' . uniqid() . '.' .$request->option_1_image->getClientOriginalExtension();

            if (!file_exists('images/questions')) {
                mkdir('images/questions', 0777, true);
            }
            $request->option_1_image->move('images/questions', $imageName1);

            /// Delete Existing Image
            if($existing_option_1_image != null){
                if (file_exists('images/questions/' . $existing_option_1_image)) {
                    unlink('images/questions/' . $existing_option_1_image);
                }
            }

            $question->option_1_image = $imageName1;
        }
        

        $question->option_2_text = $request->option_2_text;
        if ($request->hasFile('option_2_image')) { // if image is not empty.
            
            $request->validate([
                'option_2_image' => 'required|image|mimes:jpeg,png,jpg|max:2048',
            ]);

            $slug = Str::slug($request->option_2_image);

            $currentDate = Carbon::now()->toDateString();
            $imageName2 = $slug . '-' . $currentDate . '-' . uniqid() . '.' .$request->option_2_image->getClientOriginalExtension();

            if (!file_exists('images/questions')) {
                mkdir('images/questions', 0777, true);
            }
            $request->option_2_image->move('images/questions', $imageName2);

            /// Delete Existing Image
            if($existing_option_2_image != null){
                if (file_exists('images/questions/' . $existing_option_2_image)) {
                    unlink('images/questions/' . $existing_option_2_image);
                }
            }

            $question->option_2_image = $imageName2;
        }
        

        $question->option_3_text = $request->option_3_text;
        if ($request->hasFile('option_3_image')) { // if image is not empty.
           
            $request->validate([
                'option_3_image' => 'required|image|mimes:jpeg,png,jpg|max:2048',
            ]);

            $slug = Str::slug($request->option_3_image);

            $currentDate = Carbon::now()->toDateString();
            $imageName3 = $slug . '-' . $currentDate . '-' . uniqid() . '.' .$request->option_3_image->getClientOriginalExtension();

            if (!file_exists('images/questions')) {
                mkdir('images/questions', 0777, true);
            }
            $request->option_3_image->move('images/questions', $imageName3);

            /// Delete Existing Image
            if($existing_option_3_image != null){
                if (file_exists('images/questions/' . $existing_option_3_image)) {
                    unlink('images/questions/' . $existing_option_3_image);
                }
            }

            $question->option_3_image = $imageName3;
            
        }

        $question->option_4_text = $request->option_4_text;
        if ($request->hasFile('option_4_image')) { // if image is not empty.
           
            $request->validate([
                'option_4_image' => 'required|image|mimes:jpeg,png,jpg|max:2048',
            ]);

            $slug = Str::slug($request->option_4_image);

            $currentDate = Carbon::now()->toDateString();
            $imageName4 = $slug . '-' . $currentDate . '-' . uniqid() . '.' .$request->option_4_image->getClientOriginalExtension();

            if (!file_exists('images/questions')) {
                mkdir('images/questions', 0777, true);
            }
            $request->option_4_image->move('images/questions', $imageName4);

            /// Delete Existing Image
            if($existing_option_4_image != null){
                if (file_exists('images/questions/' . $existing_option_4_image)) {
                    unlink('images/questions/' . $existing_option_4_image);
                }
            }

            $question->option_4_image = $imageName4;
        }

        $question->correct_answer = $request->correct_answer;

        
        $question->updated_by = Auth::user()->id;


        $question->save();

        if ($question) {
            return redirect()->route('questions.listOfQuestions',compact('question_set_id'))->with('success', 'Question  Has Been updated Successfully');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Questions  $questions
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $question = Questions::findorFail($id);
        $question_set_id = $question->question_set_id;
 
        $existing_question_image = $question->question_image;
        $existing_option_1_image = $question->option_1_image;
        $existing_option_2_image = $question->option_2_image;
        $existing_option_3_image = $question->option_3_image;
        $existing_option_4_image = $question->option_4_image;


        /// Delete Existing Image
        if($existing_question_image != null){
            if (file_exists('images/questions/' . $existing_question_image)) {
                unlink('images/questions/' . $existing_question_image);
            }
        }

        /// Delete Existing Image
        if($existing_option_1_image != null){
            if (file_exists('images/questions/' . $existing_option_1_image)) {
                unlink('images/questions/' . $existing_option_1_image);
            }
        }

        /// Delete Existing Image
        if($existing_option_2_image != null){
            if (file_exists('images/questions/' . $existing_option_2_image)) {
                unlink('images/questions/' . $existing_option_2_image);
            }
        }


        /// Delete Existing Image
        if($existing_option_3_image != null){
            if (file_exists('images/questions/' . $existing_option_3_image)) {
                unlink('images/questions/' . $existing_option_3_image);
            }
        }


        /// Delete Existing Image
        if($existing_option_4_image != null){
            if (file_exists('images/questions/' . $existing_option_4_image)) {
                unlink('images/questions/' . $existing_option_4_image);
            }
        }


        
        $question->status = 0;
        $question->save();

        return redirect()->route('questions.listOfQuestions',compact('question_set_id'))->with('success', 'Question  Has Been Deleted Successfully');
    }

    /// Question List

    public function listOfQuestions($question_set_id)
    {
        $questions = Questions::where([['question_set_id', '=', $question_set_id], ['status', '=', 1]])->get();

        $questionSet = QuestionSet::findorFail($question_set_id);
       
        return view('questions.index', compact('questions','question_set_id','questionSet'));
    }

    public function createQuestion($question_set_id)
    {
        //
        $questionSet = QuestionSet::findorFail($question_set_id);

        return view('questions.create', compact('question_set_id','questionSet')); 
        
    }
}
