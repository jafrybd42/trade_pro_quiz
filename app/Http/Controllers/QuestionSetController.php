<?php

namespace App\Http\Controllers;

use App\Models\QuestionSet;
use App\Http\Controllers\CommonController;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class QuestionSetController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->common_class_obj = new CommonController();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $questionSets = QuestionSet::where('status', 1)->orderBy('id', 'DESC')->get();
        return view('questionSet.index', compact('questionSets'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        $validatedata = $request->validate([
            'title' => 'required|max:50|min:5',
            // 'mark' => 'required|numeric|min:1',
            // 'duration' => 'required|numeric|min:1',
        ]);

        if (QuestionSet::where([['title', '=', $request->input('title')], ['status', '=', 1]])->first()) {
            return redirect()->route('question-set.index')->with('error', 'Question Set ' . $request->input('title') . ' Already in Use');
        }

        $questionSet = new QuestionSet();
        $questionSet->title = $request->title;
        // $questionSet->total_mark = $request->mark;
        // $questionSet->duration_time = $request->duration;
        $questionSet->created_by = Auth::user()->id;
        $questionSet->updated_by = Auth::user()->id;

        

        $questionSet->save();

        if ($questionSet) {
            return redirect()->route('question-set.index')->with('success', 'Question Set ' . $request->input('title') .' Has Been Added Successfully');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\QuestionSet  $questionSet
     * @return \Illuminate\Http\Response
     */
    public function show(QuestionSet $questionSet)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\QuestionSet  $questionSet
     * @return \Illuminate\Http\Response
     */
    public function edit(QuestionSet $questionSet)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\QuestionSet  $questionSet
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\QuestionSet  $questionSet
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $questionSet = QuestionSet::findorFail($id);
        $questionSet->status = 0;
        $questionSet->save();

        return redirect()->route('question-set.index')->with('success', 'Question Set ' . $questionSet->title .' Has Been Deleted Successfully');
    }

    public function updateQuestionSet(Request $request)
    {
        $validatedata = $request->validate([
            'title' => 'required|max:50|min:5',
            // 'mark' => 'required|numeric|min:1',
            // 'duration' => 'required|numeric|min:1',
        ]);

    $questionSet = QuestionSet::findorFail($request->id);
   
    
    $existingQuestionSet = QuestionSet::where([['title', '=', $request->input('title')], ['status', '=', 1]])->first();

    if (!empty($existingQuestionSet) && $existingQuestionSet->id  != $questionSet->id) {
        return redirect()->route('question-set.index')->with('error', 'Question Set ' . $request->input('title') . ' Already in Use');
    }

    $questionSet->title = $request->title;
    // $questionSet->total_mark = $request->mark;
    // $questionSet->duration_time =  $request->duration;
    $questionSet->updated_by = Auth::user()->id;


    $questionSet->save();

        if ($questionSet) {
            return redirect()->route('question-set.index')->with('success', 'Question Set ' . $request->input('title') .' Has Been Updated Successfully');
        }
    }
}
