<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\ParticipantController;
use App\Http\Controllers\QuestionSetController;
use App\Http\Controllers\QuestionsController;
use App\Http\Controllers\BulkQuestionController;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
   // return view('welcome');
    return view('index');
});


Auth::routes();

Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('/login', [App\Http\Controllers\LoginController::class, 'index'])->name('login');
Route::get('/change/password', [App\Http\Controllers\Auth\LoginController::class, 'changePasswordUI'])->name('changePasswordUI');
Route::post('/change-password', [App\Http\Controllers\Auth\LoginController::class, 'changePasswordConfirm'])->name('changePasswordConfirm');



/// Admin Routes
Route::get('/dashboard', [App\Http\Controllers\AdminController::class,'index'])->name('dashboard')->middleware('Admin');

//Question Set
Route::resource('question-set',QuestionSetController::class)->middleware('Admin');
Route::post('/question-set/updateQuestionSet/', [QuestionSetController::class,'updateQuestionSet'])->name('question-set.updateQuestionSet')->middleware('Admin');

//Questions
Route::resource('questions',QuestionsController::class)->middleware('Admin');
Route::get('/questionList/{question_set_id}', [QuestionsController::class,'listOfQuestions'])->name('questions.listOfQuestions')->middleware('Admin');
Route::get('/question-create/{question_set_id}', [QuestionsController::class,'createQuestion'])->name('questions.createQuestion')->middleware('Admin');
Route::get('/question-upload/{question_set_id}', [BulkQuestionController::class,'uploadQuestion'])->name('questions.uploadQuestion')->middleware('Admin');
Route::post('/question-import', [BulkQuestionController::class,'importQuestions'])->name('questions.importQuestions')->middleware('Admin');

/// Participants Route
Route::get('/participant', [App\Http\Controllers\ParticipantController::class,'index'])->name('participant')->middleware('Participant');
Route::get('/participantList', [App\Http\Controllers\ParticipantController::class,'participantList'])->name('participant.participantList')->middleware('Admin');
Route::get('/participant-profile-details/{participant_id}', [ParticipantController::class,'profileDetailsForAdmin'])->name('participant.profileDetailsForAdmin')->middleware('Admin');
Route::get('/profile', [ParticipantController::class,'profile'])->name('participant.profile')->middleware('Participant');
Route::get('/practice-test/{question_set_id}', [ParticipantController::class,'questions'])->name('participant.questions')->middleware('Participant');
Route::post('/practice-exam-submit', [ParticipantController::class,'exam_submit'])->name('participant.exam-submit')->middleware('Participant');
Route::get('/participant-history/{participant_id}', [ParticipantController::class,'historyForAdmin'])->name('participant.historyForAdmin')->middleware('Admin');
Route::get('/exam-history', [ParticipantController::class,'historyForParticipant'])->name('participant.historyForParticipant')->middleware('Participant');


Route::get('/exam-material/{question_set_id}', [ParticipantController::class,'examMaterial'])->name('participant.examMaterial')->middleware('Participant');
