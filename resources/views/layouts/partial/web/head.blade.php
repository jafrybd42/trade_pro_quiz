  <meta charset="UTF-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />

  <title>Trade Pro Canada</title>

  <link rel="stylesheet" type="text/css" href="{{ asset('../frontend/assets/css/animate.css') }}" />
  <link rel="stylesheet" type="text/css" href="{{ asset('../frontend/assets/css/tp-chameleon.css') }}" />
  <link rel="stylesheet" type="text/css" href="{{ asset('../frontend/assets/css/one.css' ) }}" />
  <link rel="stylesheet" type="text/css" href="{{ asset('../frontend/assets/css/two.css') }}" />
  <link rel="stylesheet" type="text/css" href="{{ asset('../frontend/assets/css/three.css') }}" />
  <link rel="stylesheet" type="text/css" href="{{ asset('../frontend/assets/css/four.css') }} " />
  <link rel="stylesheet" type="text/css" href="{{ asset('../frontend/assets/css/five.css') }}" />
  <link rel="stylesheet" type="text/css" href="{{ asset('../frontend/assets/css/six.css') }}" />
  <link rel="stylesheet" type="text/css" href="{{ asset('../frontend/assets/css/custom.css') }}" />

  <link rel="stylesheet" type="text/css" href="{{ asset('../frontend/assets/css/animsition.min.css') }}" />

  <link rel="stylesheet" type="text/css" href="{{ asset('../frontend/assets/css/bootstrap.min.css') }}" />

  <link rel="stylesheet" type="text/css" href="{{ asset('../frontend/assets/css/jquery.lineProgressbar.css') }}" />

  <link rel="stylesheet" type="text/css" href="{{ asset('../frontend/assets/css/style-child.css') }}" />

  <link rel="stylesheet" type="text/css" href="{{ asset('../frontend/assets/css/swiper-bundle.min.css') }}" />

  <script type="text/javascript" async="" src="https://www.google.com/pagead/conversion_async.js"></script>
  
  <script type="text/javascript" async="" src="https://www.google-analytics.com/analytics.js"></script>

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>