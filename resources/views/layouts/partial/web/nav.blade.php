     <!-- Left Side Of Navbar
     <ul class="navbar-nav mr-auto">

     </ul>

    Right Side Of Navbar 
     <ul class="navbar-nav ml-auto">
         Authentication Links 
         @guest
         @if (Route::has('login'))
         <li class="nav-item">
             <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
         </li>
         @endif

         @if (Route::has('register'))
         <li class="nav-item">
             <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
         </li>
         @endif
         @else
         <li class="nav-item dropdown">
             <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                 {{ Auth::user()->name }}
             </a>

             <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                 <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                 document.getElementById('logout-form').submit();">
                     {{ __('Logout') }}
                 </a>

                 <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                     @csrf
                 </form>
             </div>
         </li>
         @endguest
     </ul> -->


     <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>
     <header class="site-header sticky-header header-overlay layout-2 fixed" id="masthead" style="top: 0px; height: 134px">
       <div class="header-inner element-to-stick" style="top: 0px">
         <div class="container">
           <div class="wrap-content-header py-0">
             <div class="header-logo">
               <a href="{{ route('home') }}" class="logo" title="online Quiz">
                 <img src="{{ asset('../frontend/assets/img/logo.png') }}" alt="online Quiz" title="online Quiz" />
               </a>
             </div>
             <nav class="main-navigation nav navbar-nav menu-main-menu">
               <ul class="menu-lists navigation">
                 <li class="menu-item-has-children menu-item active">
                   <a href="{{ route('home') }}"> Home </a>
                 </li>
                 <li class="menu-item-has-children menu-item">
                   <a href="#practice" class="btn-scroll-to-demo btn-scroll">
                     Practice Test
                   </a>
                 </li>
                 <li class="menu-item-has-children menu-item">
                   <a href="#about" class="btn-scroll-to-add-ons btn-scroll">
                     About
                   </a>
                 </li>
                 <li class="menu-item-has-children menu-item">
                   <a href="#contact"> Contact </a>
                 </li>
               </ul>
             </nav>
             <div class="menu-right">

               @if (Route::has('login'))
               <div class="hidden fixed top-0 right-0 px-6 py-4 sm:block">
                 @auth
                 
                 <div class="row custom">
                   <!-- <button class="btn btn-dark" onclick="{{ route('logout') }}">Logout</button>
                   <button class="btn btn-primary" onclick="{{ route('participant.profile') }}">Profile</button> -->
                   <form id="logout-form" action="{{ route('logout') }}" method="POST">
                     @csrf
                     <input type="submit" value="Logout" class="ctm authLink pr-3">
                   </form>
                   @if(Auth::user()->role_id == 2)
                   <a href="{{ route('participant.profile') }}" class="authLink">
                    Hi, {{ Auth::user()->first_name}}
                   </a>
                   @endif

                 </div>



                 @else

                 <aside class="widget widget_thim_layout_builder">
                   <div class="bp-element bp-element-button align-right shape-round">
                     <a href="{{ route('login') }}" class="btn btn-small">
                       Login / Register
                     </a>
                   </div>
                 </aside>

                 @endauth
               </div>

               @endif

               <div class="icon-cart">
                 <a href="#" target="_blank"></a>
               </div>
               <div class="menu-mobile-effect navbar-toggle">
                 <span class="icon-wrap"></span>
               </div>
             </div>
           </div>
         </div>
       </div>
     </header>

     <nav class="mobile-menu-container mobile-effect">
       <ul class="nav navbar-nav">
         <li>
           <a href="#home" title="online Quiz"> Home </a>
         </li>
         <li>
           <a href="#practice" class="btn-scroll-to-demo btn-scroll">
             Practice Test
           </a>
         </li>
         <li>
           <a href="#about" class="btn-scroll-to-add-ons btn-scroll">
             About
           </a>
         </li>
         <li>
           <a href="#contact" target="_blank"> Contact </a>
         </li>
         <li>
           <a href="/login" target="_blank"> Login/Registration </a>
         </li>
       </ul>
     </nav>