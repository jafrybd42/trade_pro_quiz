<div class="main-sidebar">
  <aside id="sidebar-wrapper">
    <div class="sidebar-brand">
      <a href="home">Trade Pro Canada</a>
    </div>
    <div class="sidebar-brand sidebar-brand-sm">
      <a href="home"></a>
    </div>
    <ul class="sidebar-menu">
      
         
      

      <li class="nav-item dropdown">
        <a href="#" class="nav-link has-dropdown" data-toggle="dropdown"><i class="fas fa-cubes"></i> <span>Question Set</span></a>
        <ul class="dropdown-menu">
          <li>
            
            <a class="nav-link" href="{{ route('question-set.index') }}">Practice Test</a>
            
          </li>
        </ul>

      </li>

      <li class="nav-item dropdown">
        <a href="#" class="nav-link has-dropdown" data-toggle="dropdown"><i class="fas fa-cubes"></i> <span>Participants</span></a>
        <ul class="dropdown-menu">
          <li>
            
            <a class="nav-link" href="{{ route('participant.participantList') }}">Participant List</a>
            
          </li>
        </ul>

      </li>


  </aside>

</div>