<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <title>Login / Register</title>
  <link rel="stylesheet" type="text/css" href="{{ asset('../frontend/assets/css/animate.css') }}" />
  <link rel="stylesheet" type="text/css" href="{{ asset('../frontend/assets/css/tp-chameleon.css') }}" />
  <link rel="stylesheet" type="text/css" href="{{ asset('../frontend/assets/css/one.css') }}" />
  <link rel="stylesheet" type="text/css" href="{{ asset('../frontend/assets/css/two.css') }}" />
  <link rel="stylesheet" type="text/css" href="{{ asset('../frontend/assets/css/three.css') }}" />
  <link rel="stylesheet" type="text/css" href="{{ asset('../frontend/assets/css/four.css') }}" />
  <link rel="stylesheet" type="text/css" href="{{ asset('../frontend/assets/css/five.css') }}" />
  <link rel="stylesheet" type="text/css" href="{{ asset('../frontend/assets/css/six.css') }}" />
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous" />
</head>

<body>

  <a href="{{ URL::to('/') }}">Back to Homepage</a>
  @if ($errors->any())
  <div class="col-sm-12">
    <div class="alert  alert-warning alert-dismissible fade show" role="alert">
      @foreach ($errors->all() as $error)
      <span>
        <p>{{ $error }}</p>
      </span>
      @endforeach
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
  </div>
  @endif

  @include('flash-message')
  <div class="container" id="container">
    <div class="form-container sign-up-container">
      <form method="POST" action="{{ route('register') }}">
        @csrf
        <h1>Sign Up</h1>

        <div class="row mt-4">
          <div class="col-lg-6">
            <input type="text" placeholder="First Name" class="form-control @error('first_name') is-invalid @enderror" name="first_name" value="{{ old('first_name') }}" required autocomplete="first_name" autofocus>

            @error('first_name')
            <span class="invalid-feedback" role="alert">
              <strong>{{ $message }}</strong>
            </span>
            @enderror
          </div>
          <div class="col-lg-6">
            <input type="text" placeholder="Last Name" class="form-control @error('last_name') is-invalid @enderror" name="last_name" value="{{ old('last_name') }}" required autocomplete="last_name" autofocus>

            @error('last_name')
            <span class="invalid-feedback" role="alert">
              <strong>{{ $message }}</strong>
            </span>
            @enderror
          </div>
          <div class="col-lg-6">
            <input id="email" placeholder="Email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

            @error('email')
            <span class="invalid-feedback" role="alert">
              <strong>{{ $message }}</strong>
            </span>
            @enderror
          </div>
          <div class="col-lg-6">
            <input id="phone" type="text" placeholder="Phone Number" class="form-control @error('phone') is-invalid @enderror" name="phone" value="{{ old('phone') }}" required autocomplete="phone">
            @error('phone_no')
            <span class="invalid-feedback" role="alert">
              <strong>{{ $message }}</strong>
            </span>
            @enderror
          </div>
        </div>
        <input type="text" placeholder="Address" class="form-control @error('address') is-invalid @enderror" name="address" value="{{ old('address') }}" required autocomplete="address" autofocus>

        @error('address')
        <span class="invalid-feedback" role="alert">
          <strong>{{ $message }}</strong>
        </span>
        @enderror
        <div class="row">
          <div class="col-lg-6">
            <input type="text" placeholder="City" class="form-control @error('city') is-invalid @enderror" name="city" value="{{ old('city') }}" required autocomplete="city" autofocus>

            @error('city')
            <span class="invalid-feedback" role="alert">
              <strong>{{ $message }}</strong>
            </span>
            @enderror
          </div>
          <div class="col-lg-6">
            <input type="text" placeholder="State" class="form-control @error('state') is-invalid @enderror" name="state" value="{{ old('state') }}" required autocomplete="state" autofocus>

            @error('state')
            <span class="invalid-feedback" role="alert">
              <strong>{{ $message }}</strong>
            </span>
            @enderror
          </div>
        </div>
        <input type="text" placeholder="Postal / Zip Code" class="row form-control @error('postal_code') is-invalid @enderror" name="postal_code" value="{{ old('postal_code') }}" required autocomplete="postal_code" autofocus>

        @error('postal_code')
        <span class="invalid-feedback" role="alert">
          <strong>{{ $message }}</strong>
        </span>
        @enderror

        <div class="row">
          <div class="col-lg-6">
            <input id="password" type="password" placeholder="Password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

            @error('password')
            <span class="invalid-feedback" role="alert">
              <strong>{{ $message }}</strong>
            </span>
            @enderror
          </div>
          <div class="col-lg-6">
            <input id="password-confirm" type="password" class="form-control" placeholder="Confirm Password" name="password_confirmation" required autocomplete="new-password">
          </div>
        </div>

        <button type="submit" class="mt-2">
          {{ __('Register') }}
        </button>

      </form>
    </div>
    <div class="form-container sign-in-container">
      <form method="POST" action="{{ route('login') }}">
        @csrf
        <h1>Sign In</h1>
        <input id="email" placeholder="Email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

        <!-- <input type="email" placeholder="Email" /> -->

        @error('email')
        <span class="invalid-feedback" role="alert">
          <strong>{{ $message }}</strong>
        </span>
        @enderror


        <!-- <input type="password" placeholder="Password" /> -->

        <input id="password" type="password" placeholder="Password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

        @error('password')
        <span class="invalid-feedback" role="alert">
          <strong>{{ $message }}</strong>
        </span>
        @enderror

        {{-- <a href="#">Forgot your password?</a> --}}
        <button type="submit">
          {{ __('Login') }}
        </button>

        @if (Route::has('password.request'))
        <a class="btn btn-link" href="{{ route('password.request') }}">
          {{ __('Forgot Your Password?') }}
        </a>
        @endif
      </form>
    </div>
    <div class="overlay-container">
      <div class="overlay">
        <div class="overlay-panel overlay-left">
          <h1>Welcome Back!</h1>
          <p>Please login with your personal info</p>
          <button class="ghost" id="signIn">Sign In</button>
        </div>
        <div class="overlay-panel overlay-right">
          <h1>Hello, Friend!</h1>
          <p>Enter your personal details and start your journey with us</p>
          <button class="ghost" id="signUp">Sign Up</button>
        </div>
      </div>
    </div>
  </div>
</body>

<style>
  @import url("https://fonts.googleapis.com/css2?family=Nunito:wght@400;800&display=swap");

  :root {
    --main-color: #d90e17;
    --secondary-color: #b02135;
    --gradient: linear-gradient(135deg,
        var(--main-color),
        var(--secondary-color));
  }

  * {
    box-sizing: border-box;
  }

  body {
    background: #f6f5f7;
    font-family: "Nunito", sans-serif;
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    min-height: 100vh;
    margin: 1rem 0;
  }

  h1 {
    font-weight: bold;
    margin: 0;
  }

  p {
    font-size: 14px;
    font-weight: 100;
    line-height: 20px;
    letter-spacing: 0.5px;
    margin: 20px 0 30px;
  }

  .social-container {
    margin: 20px 0;
  }

  .social-container a {
    border: 1px solid #dddddd;
    border-radius: 50%;
    display: inline-flex;
    justify-content: center;
    align-items: center;
    margin: 0 5px;
    height: 40px;
    width: 40px;
  }

  span {
    font-size: 12px;
  }

  a {
    color: #333;
    font-size: 14px;
    text-decoration: none;
    margin: 15px 0;
  }

  button {
    cursor: pointer;
    border-radius: 20px;
    border: 1px solid var(--main-color);
    background: var(--main-color);
    color: #fff;
    font-size: 12px;
    font-weight: bold;
    padding: 12px 45px;
    letter-spacing: 1px;
    text-transform: uppercase;
    transition: transform 80ms ease-out;
  }

  button:hover {
    background: var(--secondary-color);
  }

  button:active {
    transform: scale(0.95);
  }

  button:focus {
    outline: none;
  }

  button.ghost {
    background-color: transparent;
    border-color: #fff;
  }

  button.ghost:hover {
    background: #fff;
    color: var(--secondary-color);
  }

  form {
    background-color: #fff;
    display: flex;
    align-items: center;
    justify-content: center;
    flex-direction: column;
    padding: 0 50px;
    height: 100%;
    text-align: center;
  }

  input {
    background-color: #eee;
    border: none;
    padding: 12px 15px;
    margin: 8px 0;
    width: 100%;
    font-family: inherit;
  }

  .container {
    background-color: #fff;
    border-radius: 10px;
    box-shadow: 0 14px 28px rgba(0, 0, 0, 0.25),
      0 10px 10px rgba(0, 0, 0, 0.22);
    position: relative;
    overflow: hidden;
    /* height: 768px;
    width: 480px;
    max-width: 100%; */
  }

  .container {
    width: 1200px !important;
    max-width: 100% !important;
    height: 600px !important;
  }

  .form-container {
    position: absolute;
    top: 0;
    width: 100%;
    transition: all 0.6s ease-in-out;
  }

  .sign-in-container {
    top: 0;
    height: 50%;
    z-index: 2;
  }

  .container.right-panel-active .sign-in-container {
    transform: translateY(100%);
  }

  .sign-up-container {
    top: 0;
    height: 50%;
    opacity: 0;
    z-index: 1;
  }

  .container.right-panel-active .sign-up-container {
    transform: translateY(100%);
    opacity: 1;
    z-index: 5;
    animation: show 0.6s;
  }

  @keyframes show {

    0%,
    49.99% {
      opacity: 0;
      z-index: 1;
    }

    50%,
    100% {
      opacity: 1;
      z-index: 5;
    }
  }

  .overlay-container {
    position: absolute;
    left: 0;
    top: 50%;
    height: 50%;
    width: 100%;
    overflow: hidden;
    transition: transform 0.6s ease-in-out;
    z-index: 100;
  }

  .container.right-panel-active .overlay-container {
    transform: translateY(-100%);
  }

  .overlay {
    background: var(--secondary-color);
    background: var(--gradient);
    background-repeat: no-repeat;
    background-size: cover;
    background-position: 0 0;
    color: #fff;
    position: relative;
    top: -100%;
    width: 100%;
    height: 200%;
    transform: translateY(0);
    transition: transform 0.6s ease-in-out;
  }

  .container.right-panel-active .overlay {
    transform: translateY(50%);
  }

  .overlay-panel {
    position: absolute;
    display: flex;
    align-items: center;
    justify-content: center;
    flex-direction: column;
    padding: 0 40px;
    text-align: center;
    left: 0;
    width: 100%;
    height: 50%;
    transform: translateY(0);
    transition: transform 0.6s ease-in-out;
  }

  .overlay-left {
    transform: translateY(-20%);
  }

  .container.right-panel-active .overlay-left {
    transform: translateY(0);
  }

  .overlay-right {
    bottom: 0;
    transform: translateY(0);
  }

  .container.right-panel-active .overlay-right {
    transform: translateY(20%);
  }

  @media (min-width: 768px) {
    body {
      margin: -20px 0 50px;
    }

    .container {
      width: 768px;
      max-width: 100%;
      height: 480px;
    }

    .form-container {
      top: 0;
      height: 100%;
      width: 50%;
    }

    .sign-in-container {
      left: 0;
      width: 50%;
      height: 100%;
    }

    .container.right-panel-active .sign-in-container {
      transform: translateX(100%);
    }

    .sign-up-container {
      left: 0;
      width: 50%;
      height: 100%;
    }

    .container.right-panel-active .sign-up-container {
      transform: translateX(100%);
    }

    .overlay-container {
      left: 50%;
      top: 0;
      height: 100%;
      width: 50%;
    }

    .container.right-panel-active .overlay-container {
      transform: translateX(-100%);
    }

    .overlay {
      top: 0;
      left: -100%;
      height: 100%;
      width: 200%;
      transform: translateX(0);
    }

    .container.right-panel-active .overlay {
      transform: translateX(50%);
    }

    .overlay-panel {
      top: 0;
      height: 100%;
      width: 50%;
      transform: translateX(0);
    }

    .overlay-left {
      transform: translateX(-20%);
    }

    .container.right-panel-active .overlay-left {
      transform: translateX(0);
    }

    .overlay-right {
      right: 0;
      top: 0;
      left: 50%;
      transform: translateX(0);
    }

    .container.right-panel-active .overlay-right {
      transform: translateX(20%);
    }
  }

  @media (max-width: 768px) {
    .container {
      height: 1500px !important;
    }
  }
</style>
<script>
  const signUpButton = document.getElementById("signUp");
  const signInButton = document.getElementById("signIn");
  const container = document.getElementById("container");

  signUpButton.addEventListener("click", () => {
    container.classList.add("right-panel-active");
  });

  signInButton.addEventListener("click", () => {
    container.classList.remove("right-panel-active");
  });
</script>

<!--  -->

<script src="{{ asset('../frontend/assets/js/script.js') }}"></script>

<script src="{{ asset('../frontend/assets/js/script2.js') }}"></script>

<script src="{{ asset('../frontend/"assets/js/script3.js') }}"></script>

<script src="{{ asset('../frontend/assets/js/script4.js') }}"></script>

<script src="{{ asset('../frontend/assets/js/script5.js') }}"></script>

<script src="{{ asset('../frontend/assets/js/script6.js') }}"></script>

<script src="{{ asset('../frontend/assets/js/script7.js') }}"></script>

<script src="{{ asset('../frontend/assets/js/script8.js') }}" min=""></script>

<style>
  .tb_button {
    padding: 1px;
    cursor: pointer;
    border-right: 1px solid #8b8b8b;
    border-left: 1px solid #fff;
    border-bottom: 1px solid #fff;
  }

  .tb_button.hover {
    borer: 2px outset #def;
    background-color: #f8f8f8 !important;
  }

  .ws_toolbar {
    z-index: 100000;
  }

  .ws_toolbar .ws_tb_btn {
    cursor: pointer;
    border: 1px solid #555;
    padding: 3px;
  }

  .tb_highlight {
    background-color: yellow;
  }

  .tb_hide {
    visibility: hidden;
  }

  .ws_toolbar img {
    padding: 2px;
    margin: 0px;
  }
</style>

</html>