@extends('layouts.app')

@section('content')
<div>
  <div class="thim-block-slider-home" id="home" style="background: #c10d0de6">
    <div class="container">
      <div class="row">
        <div class="col-lg-6 content">
          <div class="slider-img">
            <div class="img-text animated fadeInLeft pt-4">
              <img src="{{ asset('../frontend/assets/img/one.png') }}" alt="Online Quiz" />
            </div>
          </div>
          <div class="slider-text">
            <div class="content-text">
              <!-- <h3 class="title-silde animated fadeInDown">
                      The best for online education
                    </h3> -->
              <h1 class="title-lms animated fadeInRightBig">
                Online Quiz
              </h1>
              <div class="button-eduma animated fadeInUp">
                <a href="#practice" class="btn btn-slide-left">PREP FOR TRADE EXAM
                </a>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-6 content justify-content-center">
          <div class="slider-text">
            <img src="{{ asset('../frontend/assets/img/online-test.png') }}" />
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="thim-block-plugin-home" id="practice">
  <div class="container-content">
    <div class="row-content">
      <div class="col-lg-4 content-text">
        <h3 class="title-plugin wow fadeInLeft" style="visibility: visible; animation-name: fadeInLeft">
          Please Choose Your Test from Available
        </h3>
        <p class="wow fadeInUp" style="visibility: visible; animation-name: fadeInUp">
          Engage your audience in a unique and fun way and connect them to
          your brand or learning material.
        </p>
      </div>
      <div class="col-lg-8 content-img">
        @foreach ($questionSets as $key => $questionSet)
        <div class="col-lg-4 col-md-6 pd-content wow fadeInUp" style="visibility: visible; animation-name: fadeInUp">
          @if($questionSet['willShowTOExam'] == true )
          <a href="{{ route('participant.questions', $questionSet->id) }}" class="">
            <div class="content-img-plugin">
              <h2>
                <!-- <a target="_blank" rel="noopener noreferrer"> -->
                  <strong>{{ $questionSet->title }}</strong>
                <!-- </a> -->
              </h2>
              <div class="hover-img-plugin">
                <!-- <a href="#scroll-to-demo" class="btn-scroll"> -->
                <!-- <div class="btn-scroll"> -->
                  <img src="{{ asset('../frontend/assets/img/redseal.png') }}" class="btImg" title="Eduma Demos" />
                <!-- </a> -->
              <!-- </div> -->
              </div>
             
              <!-- <a href="{{ route('participant.questions', $questionSet->id) }}" class=""> -->
              <h3>
                <strong>Start Test</strong>
              </h3>
              <!-- </a> -->
            </div>
          </a>
              @else
              <a href="" class="">
                <div class="content-img-plugin">
                  <h2>
                    <!-- <a target="_blank" rel="noopener noreferrer"> -->
                      <strong>{{ $questionSet->title }}</strong>
                    <!-- </a> -->
                  </h2>
                  <div class="hover-img-plugin">
                    <!-- <a href="#scroll-to-demo" class="btn-scroll"> -->
                    <!-- <div class="btn-scroll"> -->
                      <img src="{{ asset('../frontend/assets/img/redseal.png') }}" class="btImg" title="Eduma Demos" />
                    <!-- </a> -->
                  <!-- </div> -->
                  </div>
                 
                  <!-- <a href="{{ route('participant.questions', $questionSet->id) }}" class=""> -->
                    <h3>
                      <strong>Not Available</strong>
                    </h3>
                  <!-- </a> -->
                </div>
              </a>
              @endif
              
              
            </div>
          

        @endforeach

      </div>
    </div>
  </div>
</div>

<div id="about" class="thim-block-elementor-home-01" style="
          background-image: url(https://eduma.thimpress.com/wp-content/themes/eduma-child-landingpage/assets/images/bg-elementor-03.png);
        ">
  <div class="container-content-wrap">
    <div class="row-content-wrap">
      <div class="col-lg-7 wow fadeInRight" style="visibility: visible; animation-name: fadeInRight">
        <div class="content-img">
          <img src="https://eduma.thimpress.com/wp-content/themes/eduma-child-landingpage/assets/images/elementor-img-04.png" alt="Premium Widgets and Shortcodes" title="Premium Widgets and Shortcodes" />
        </div>
      </div>
      <div class="col-lg-5 wow fadeInLeft" style="visibility: visible; animation-name: fadeInLeft">
        <div class="content-box">
          <div class="content-text">
            <h3>About</h3>
            <p>
              Giving you pro-level widgets and shortcodes for design,
              layout creating, and dynamic content styling.
            </p>
          </div>
          <div class="content-check">
            <div class="box col-lg-9">
              <div class="item">Pro Widgets</div>
              <div class="item">ThimPress Shortcodes</div>
              <div class="item">Popular Marketing Tools Integration</div>
              <div class="item">Login Form and Popup Builder</div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div id="contact" class="thim-block-elementor-home-01">
  <div class="container-content-wrap">
    <div class="row-content-wrap">
      <div class="col-lg-6 wow fadeInRight" style="visibility: visible; animation-name: fadeInRight">
        <!-- <div class="content-img"> -->
        <!-- <img src="https://eduma.thimpress.com/wp-content/themes/eduma-child-landingpage/assets/images/elementor-img-02.png" alt="Theme Customize" title="Theme Customize"> -->
        <form class="my-form">
          <div class="container">
            <ul>
              <li>

              </li>
              <li>
                <div class="grid grid-2">
                  <input type="text" placeholder="Name" required />
                  <input type="text" placeholder="Surname" required />
                </div>
              </li>
              <li>
                <div class="grid grid-2">
                  <input type="email" placeholder="Email" required />
                  <input type="tel" placeholder="Phone" />
                </div>
              </li>
              <li>
                <textarea placeholder="Message"></textarea>
              </li>
              <li>
                <input type="checkbox" id="terms" />
                <label for="terms">I have read and agreed with
                  <a href="">the terms and conditions.</a></label>
              </li>
              <li>
                <div class="grid grid-3">
                  <div class="required-msg">REQUIRED FIELDS</div>
                  <button class="btn-grid" type="submit" disabled>
                    <span class="back">
                      <img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/162656/email-icon.svg" alt="" />
                    </span>
                    <span class="front">SUBMIT</span>
                  </button>
                  <button class="btn-grid" type="reset" disabled>
                    <span class="back">
                      <img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/162656/eraser-icon.svg" alt="" />
                    </span>
                    <span class="front">RESET</span>
                  </button>
                </div>
              </li>
            </ul>
          </div>
        </form>
        <!-- </div> -->
      </div>
      <div class="col-lg-6 wow fadeInLeft" style="visibility: visible; animation-name: fadeInLeft">
        <div class="content-box">
          <div class="content-text">
            <h3>Contact</h3>
            <p>
              Advanced intergration Customize allows the customization of
              places usually inaccessible with regular theme controls.
              What's more, you can add different header, footer, page
              titles, and templates to different pages...
            </p>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection





@section('extra-js')

<script src="{{ asset('../frontend/assets/js/script.js') }}"></script>

<script src="{{ asset('../frontend/assets/js/script2.js') }}"></script>

<script src="{{ asset('../frontend/assets/js/script3.js') }}"></script>

<script src="{{ asset('../frontend/assets/js/script4.js') }}"></script>

<script src="{{ asset('../frontend/assets/js/script5.js') }}"></script>

<script src="{{ asset('../frontend/assets/js/script6.js') }}"></script>

<script src="{{ asset('../frontend/assets/js/script7.js') }}"></script>

<script src="{{ asset('../frontend/assets/js/script8.js') }}" min=""></script>

<style>
  .tb_button {
    padding: 1px;
    cursor: pointer;
    border-right: 1px solid #8b8b8b;
    border-left: 1px solid #fff;
    border-bottom: 1px solid #fff;
  }

  .tb_button.hover {
    borer: 2px outset #def;
    background-color: #f8f8f8 !important;
  }

  .ws_toolbar {
    z-index: 100000;
  }

  .ws_toolbar .ws_tb_btn {
    cursor: pointer;
    border: 1px solid #555;
    padding: 3px;
  }

  .tb_highlight {
    background-color: yellow;
  }

  .tb_hide {
    visibility: hidden;
  }

  .ws_toolbar img {
    padding: 2px;
    margin: 0px;
  }
</style>
<style>
  @import url("https://fonts.googleapis.com/css?family=Open+Sans:400,700");

  /* RESET RULES
    –––––––––––––––––––––––––––––––––––––––––––––––––– */
  :root {
    --white: #bdbbbb;
    --red: #e31b23;
    /* --bodyColor: rgb(255, 255, 255); */
    --borderFormEls: hsl(0, 0%, 0%);
    /* --bgFormEls: hsl(0, 100%, 98%); */
    --bgFormElsFocus: hsl(0, 0%, 0%);
  }

  a {
    color: inherit;
  }

  input,
  select,
  textarea,
  button {
    font-family: inherit;
    font-size: 100%;
  }

  button,
  label {
    cursor: pointer;
  }

  select {
    appearance: none;
  }

  /* Remove native arrow on IE */
  select::-ms-expand {
    display: none;
  }

  /*Remove dotted outline from selected option on Firefox*/
  /*https://stackoverflow.com/questions/3773430/remove-outline-from-select-box-in-ff/18853002#18853002*/
  /*We use !important to override the color set for the select on line 99*/
  select:-moz-focusring {
    color: transparent !important;
    text-shadow: 0 0 0 var(--white);
  }

  textarea {
    resize: none;
  }

  ul {
    list-style: none;
  }

  /* 
    body {
      font: 18px/1.5 "Open Sans", sans-serif;
      background: var(--bodyColor);
      color: var(--white);
      margin: 1.5rem 0;
    } */

  /* .container {
      max-width: 800px;
      margin: 0 auto;
      padding: 0 1.5rem;
    } */

  /* FORM ELEMENTS  
    –––––––––––––––––––––––––––––––––––––––––––––––––– */
  .my-form h1 {
    margin-bottom: 1.5rem;
  }

  .my-form li,
  .my-form .grid>*:not(:last-child) {
    margin-bottom: 1.5rem;
  }

  .my-form select,
  .my-form input,
  .my-form textarea,
  .my-form button {
    width: 100%;
    line-height: 1.5;
    padding: 15px 10px;
    border: 1px solid var(--borderFormEls);
    color: var(--white);
    background: var(--bgFormEls);
    transition: background-color 0.3s cubic-bezier(0.57, 0.21, 0.69, 1.25),
      transform 0.3s cubic-bezier(0.57, 0.21, 0.69, 1.25);
  }

  .my-form textarea {
    height: 170px;
  }

  .my-form ::placeholder {
    color: inherit;
    /*Fix opacity issue on Firefox*/
    opacity: 1;
  }

  .my-form select:focus,
  .my-form input:focus,
  .my-form textarea:focus,
  .my-form button:enabled:hover,
  .my-form button:focus,
  .my-form input[type="checkbox"]:focus+label {
    background: var(--bgFormElsFocus);
  }

  .my-form select:focus,
  .my-form input:focus,
  .my-form textarea:focus {
    transform: scale(1.02);
  }

  .my-form *:required,
  .my-form select {
    background-repeat: no-repeat;
    background-position: center right 12px;
    background-size: 15px 15px;
  }

  .my-form *:required {
    background-image: url(https://s3-us-west-2.amazonaws.com/s.cdpn.io/162656/asterisk.svg);
  }

  .my-form select {
    background-image: url(https://s3-us-west-2.amazonaws.com/s.cdpn.io/162656/down.svg);
  }

  .my-form *:disabled {
    cursor: default;
    filter: blur(2px);
  }

  /* FORM BTNS
    –––––––––––––––––––––––––––––––––––––––––––––––––– */
  .my-form .required-msg {
    display: none;
    background: url(https://s3-us-west-2.amazonaws.com/s.cdpn.io/162656/asterisk.svg) no-repeat center left / 15px 15px;
    padding-left: 20px;
  }

  .my-form .btn-grid {
    position: relative;
    overflow: hidden;
    transition: filter 0.2s;
  }

  .my-form button {
    font-weight: bold;
  }

  .my-form button>* {
    display: inline-block;
    width: 100%;
    transition: transform 0.4s ease-in-out;
  }

  .my-form button .back {
    position: absolute;
    left: 50%;
    top: 50%;
    transform: translate(-110%, -50%);
  }

  .my-form button:enabled:hover .back,
  .my-form button:focus .back {
    transform: translate(-50%, -50%);
  }

  .my-form button:enabled:hover .front,
  .my-form button:focus .front {
    transform: translateX(110%);
  }

  /* CUSTOM CHECKBOX
    –––––––––––––––––––––––––––––––––––––––––––––––––– */
  .my-form input[type="checkbox"] {
    position: absolute;
    left: -9999px;
  }

  .my-form input[type="checkbox"]+label {
    position: relative;
    display: inline-block;
    padding-left: 2rem;
    transition: background 0.3s cubic-bezier(0.57, 0.21, 0.69, 1.25);
  }

  .my-form input[type="checkbox"]+label::before,
  .my-form input[type="checkbox"]+label::after {
    content: "";
    position: absolute;
  }

  .my-form input[type="checkbox"]+label::before {
    left: 0;
    top: 6px;
    width: 18px;
    height: 18px;
    border: 2px solid var(--white);
  }

  .my-form input[type="checkbox"]:checked+label::before {
    background: var(--red);
  }

  .my-form input[type="checkbox"]:checked+label::after {
    left: 7px;
    top: 7px;
    width: 6px;
    height: 14px;
    border-bottom: 2px solid var(--white);
    border-right: 2px solid var(--white);
    transform: rotate(45deg);
  }

  /* FOOTER
    –––––––––––––––––––––––––––––––––––––––––––––––––– */
  footer {
    font-size: 1rem;
    text-align: right;
    backface-visibility: hidden;
  }

  footer a {
    text-decoration: none;
  }

  footer span {
    color: var(--red);
  }

  /* MQ
    –––––––––––––––––––––––––––––––––––––––––––––––––– */
  @media screen and (min-width: 600px) {
    .my-form .grid {
      display: grid;
      grid-gap: 1.5rem;
    }

    .my-form .grid-2 {
      grid-template-columns: 1fr 1fr;
    }

    .my-form .grid-3 {
      grid-template-columns: auto auto auto;
      align-items: center;
    }

    .my-form .grid>*:not(:last-child) {
      margin-bottom: 0;
    }

    .my-form .required-msg {
      display: block;
    }
  }

  @media screen and (min-width: 541px) {
    .my-form input[type="checkbox"]+label::before {
      top: 50%;
      transform: translateY(-50%);
    }

    .my-form input[type="checkbox"]:checked+label::after {
      top: 3px;
    }
  }
</style>
<script>
  const checkbox = document.querySelector('.my-form input[type="checkbox"]');
  const btns = document.querySelectorAll(".my-form button");

  checkbox.addEventListener("change", function() {
    const checked = this.checked;
    for (const btn of btns) {
      checked ? (btn.disabled = false) : (btn.disabled = true);
    }
  });
</script>
@endsection

</body>


</html>