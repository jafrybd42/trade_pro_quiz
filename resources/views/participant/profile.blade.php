@extends('layouts.app')
@section('title', 'Question Set')
@section('extra-css')
<!-- Google Fonts -->
<link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,900&display=swap" rel="stylesheet">
<!-- Bootstrap CSS -->
<link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css'>
<!-- Font Awesome CSS -->
<link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.1/css/all.min.css'>

@endsection

@section('content')
<!-- <br>
<br>
<br>
<br>
<br>
<br>
<br>
<br> -->


<!-- Main Content -->
<div class="main-content">
    <section class="section">

        <div class="thim-block-plugin-home" id="practice">
            <div class="container-content">
                <div class="row-content">
                    <div class="col-lg-4 content-text">
                        <h3 class="title-plugin wow fadeInLeft" style="visibility: visible; animation-name: fadeInLeft">
                            Profile Details
                        </h3>
                        <a class="btn btn-primary mt-5" href="{{ route('changePasswordUI') }}">
                            <i class="fas fa-sign-out-alt"></i> {{ __('Change Password') }}
                        </a>

                        <a class="btn btn-primary mt-5" href="{{ route('participant.historyForParticipant') }}">
                            <i class="fas fa-sign-out-alt"></i> {{ __('Test History') }}
                        </a>
                    </div>
                    <div class="col-lg-8 content-img">
                        <!-- <div class="col-lg-4 col-md-6 pd-content wow fadeInUp" style="visibility: visible; animation-name: fadeInUp">
                            <div class="content-img-plugin">
                                <h2>
                                    <a target="_blank" rel="noopener noreferrer"><strong>Instrumentation and Control Technician</strong></a>
                                </h2>
                                <div class="hover-img-plugin">
                                    <a href="#scroll-to-demo" class="btn-scroll">
                                        <img src="http://127.0.0.1:8000/../frontend/assets/img/redseal.png" title="Eduma Demos">
                                    </a>
                                </div>
                                <h3>
                                    <a href="http://127.0.0.1:8000/practice-test/3" class="">
                                        <strong>Start Test</strong>
                                    </a>
                                </h3>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 pd-content wow fadeInUp" style="visibility: visible; animation-name: fadeInUp">
                            <div class="content-img-plugin">
                                <h2>
                                    <a target="_blank" rel="noopener noreferrer"><strong>Industrial Electrician</strong></a>
                                </h2>
                                <div class="hover-img-plugin">
                                    <a href="#scroll-to-demo" class="btn-scroll">
                                        <img src="http://127.0.0.1:8000/../frontend/assets/img/redseal.png" title="Eduma Demos">
                                    </a>
                                </div>
                                <h3>
                                    <a href="http://127.0.0.1:8000/practice-test/4" class="">
                                        <strong>Start Test</strong>
                                    </a>
                                </h3>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 pd-content wow fadeInUp" style="visibility: visible; animation-name: fadeInUp">
                            <div class="content-img-plugin">
                                <h2>
                                    <a target="_blank" rel="noopener noreferrer"><strong>Plumber</strong></a>
                                </h2>
                                <div class="hover-img-plugin">
                                    <a href="#scroll-to-demo" class="btn-scroll">
                                        <img src="http://127.0.0.1:8000/../frontend/assets/img/redseal.png" title="Eduma Demos">
                                    </a>
                                </div>
                                <h3>
                                    <a href="http://127.0.0.1:8000/practice-test/5" class="">
                                        <strong>Start Test</strong>
                                    </a>
                                </h3>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 pd-content wow fadeInUp" style="visibility: visible; animation-name: fadeInUp">
                            <div class="content-img-plugin">
                                <h2>
                                    <a target="_blank" rel="noopener noreferrer"><strong>Construction Electrician</strong></a>
                                </h2>
                                <div class="hover-img-plugin">
                                    <a href="#scroll-to-demo" class="btn-scroll">
                                        <img src="http://127.0.0.1:8000/../frontend/assets/img/redseal.png" title="Eduma Demos">
                                    </a>
                                </div>
                                <h3>
                                    <a href="http://127.0.0.1:8000/practice-test/6" class="">
                                        <strong>Start Test</strong>
                                    </a>
                                </h3>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 pd-content wow fadeInUp" style="visibility: visible; animation-name: fadeInUp">
                            <div class="content-img-plugin">
                                <h2>
                                    <a target="_blank" rel="noopener noreferrer"><strong>Automotive Technician</strong></a>
                                </h2>
                                <div class="hover-img-plugin">
                                    <a href="#scroll-to-demo" class="btn-scroll">
                                        <img src="http://127.0.0.1:8000/../frontend/assets/img/redseal.png" title="Eduma Demos">
                                    </a>
                                </div>
                                <h3>
                                    <a href="http://127.0.0.1:8000/practice-test/7" class="">
                                        <strong>Start Test</strong>
                                    </a>
                                </h3>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 pd-content wow fadeInUp" style="visibility: visible; animation-name: fadeInUp">
                            <div class="content-img-plugin">
                                <h2>
                                    <a target="_blank" rel="noopener noreferrer"><strong>Welder</strong></a>
                                </h2>
                                <div class="hover-img-plugin">
                                    <a href="#scroll-to-demo" class="btn-scroll">
                                        <img src="http://127.0.0.1:8000/../frontend/assets/img/redseal.png" title="Eduma Demos">
                                    </a>
                                </div>
                                <h3>
                                    <a href="http://127.0.0.1:8000/practice-test/8" class="">
                                        <strong>Start Test</strong>
                                    </a>
                                </h3>
                            </div>
                        </div> -->
                        @if ($errors->any())
                        <div class="col-sm-12">
                            <div class="alert  alert-warning alert-dismissible fade show" role="alert">
                                @foreach ($errors->all() as $error)
                                <span>
                                    <p>{{ $error }}</p>
                                </span>
                                @endforeach
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        </div>
                        @endif

                        @include('flash-message')

                        <!-- <div class="main-content">

            <div class="container-fluid mt--7">
                <div class="row">
                    <a class="dropdown-item has-icon" href="{{ route('changePasswordUI') }}">
                        <i class="fas fa-sign-out-alt"></i> {{ __('Change Password') }}
                    </a>
                    <div class="col-xl-8 order-xl-1">
                        <div class="card bg-secondary shadow">

                            <div class="card-body"> -->

                        <form>
                            <!-- <h6 class="heading-small text-muted mb-4">Profile</h6> -->
                            <div class="pl-lg-4">

                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group focused">
                                            <label class="form-control-label" for="input-first-name">First
                                                name</label>
                                            <input type="text" id="input-first-name" class="form-control form-control-alternative" value="{{ $participant->first_name }}" readonly>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group focused">
                                            <label class="form-control-label" for="input-last-name">Last
                                                name</label>
                                            <input type="text" id="input-last-name" class="form-control form-control-alternative" value="{{ $participant->last_name }}" readonly>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group focused">
                                            <label class="form-control-label" for="input-username">Contact
                                                Number</label>
                                            <input type="text" id="input-username" class="form-control form-control-alternative" value="{{ $participant->phone }}" readonly>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class="form-control-label" for="input-email">Email
                                                address</label>
                                            <input type="email" id="input-email" class="form-control form-control-alternative" value="{{ $participant->email }}" readonly>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <hr class="my-4">
                            <!-- Address -->

                            <div class="pl-lg-4">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group focused">
                                            <label class="form-control-label" for="input-address">Address</label>
                                            <input id="input-address" class="form-control form-control-alternative" type="text" value="{{ $participant->address }}" readonly>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="form-group focused">
                                            <label class="form-control-label" for="input-city">City</label>
                                            <input type="text" id="input-city" class="form-control form-control-alternative" value="{{ $participant->city }}" readonly>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group focused">
                                            <label class="form-control-label" for="input-country">State</label>
                                            <input type="text" id="input-country" class="form-control form-control-alternative" value="{{ $participant->state }}" readonly>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label class="form-control-label" for="input-country">Postal
                                                code</label>
                                            <input type="text" id="input-postal-code" class="form-control form-control-alternative" value="{{ $participant->postal_code }}" readonly>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- Description -->

                        </form>
                        <!-- </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> -->
                    </div>
                </div>
            </div>
        </div>




    </section>



</div>

@endsection

@section('extra-js')
<script>

</script>
@endsection