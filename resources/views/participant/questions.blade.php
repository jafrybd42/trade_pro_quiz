@extends('layouts.app')
@section('title', 'Question Set')
@section('extra-css')
<link href="https://fonts.googleapis.com/css?family=Bree+Serif|Concert+One|Nunito" rel="stylesheet">
<!-- <link rel="stylesheet" type="text/css" href="final.css"> -->
<style>
    /* h1 {
            margin: 60px 60px 0 60px;
            font-size: 30px;
            padding-bottom: 30px;
            border-bottom: 1px solid #b5b5b7;
            color: #ffcc00;
        } */

    /* h1, */
    /* h2 {
            text-align: center;
            font-weight: bold;
            letter-spacing: 10px;
            text-transform: uppercase;
        }

        h2 {
            font-size: 20px;
        } */
    button {
        margin-top: 20px;
        width: 200px;
        height: 40px;
        background: #c72424;
        font-family: 'Dosis', sans-serif;
        font-size: 30px;
        color: #ff4040;
        border: .5px solid #c72424;
    }

    .start-quiz {
        width: 500px;
        height: 300px;
        margin: 100px auto;
        /* text-align: center; */
        padding-bottom: 200px;
        color: white;
    }

    .cp {
        display: none;
    }

    .questions,
    .end-quiz {
        display: none;
        text-align: center;
        padding-bottom: 200px;
        margin: 100px auto 0 auto;
        text-align: center;
        width: 500px;
        height: 300px;
        margin-bottom: 100px;
    }

    .end-quiz {
        border: 2px solid #c72424;
        padding-top: 80px;
        height: 100px;
        font-size: 30px;
        color: #c72424;
    }

    .question-number,
    .score {
        font-size: 30px;
        color: #b5b5b7;
    }

    .retake-button {
        min-height: 60px;
        color: white;
    }

    ul {
        display: flex;
        width: 100%;
        list-style: none;
        padding: 0;
    }

    .score {
        font-family: Helvetica, Arial, sans-serif;
        display: inline-block;
        max-width: 100%;
        font-weight: 700;
        font-size: 16px;
        line-height: 23px;
        color: #333;
    }

    .list li {
        border: 1px solid #d6d6cf;
        width: 44%;
        margin: 15px 15px;
        border-radius: 5px;
        padding: 15px;
        text-align: center;
        height: auto;
        color: #000;
        display: inline-block;
        max-width: 100%;
        margin-bottom: 5px;
        font-weight: 700;
        font-family: Helvetica, Arial, sans-serif;
        font-size: 16px;
        line-height: 23px;
        cursor: pointer;
    }

    .list li:hover {
        background: #f2b632;
        border: 1px solid #f2b632;
        color: #000;
    }

    .start-button {
        color: white;
    }


    .selected {
        background: #f2b632;
        color: #252839;
    }

    .correct {
        background-color: green;
        border: 1px solid green !important;
        color: white !important;
        padding: 10px 20px !important;
        margin: 10px !important;
    }

    .incorrect {
        background-color: red;
        border: 1px solid red !important;
        color: white !important;
        padding: 10px 20px !important;
        margin: 10px !important;
    }

    .dot {
        height: 300px;
        width: 300px;
        border-radius: 50%;
        display: inline-block;
    }

    #about {
        margin-top: 100px;
    }

    .comment {
        color: black;
    }

    .end-score {
        color: black;
    }
</style>

@endsection

@section('content')

 @php

     $parentTitle =  App\Models\QuestionSet::select('title')->where([['id', '=', $questionSet->cat_id], ['status', '=', 1]])->first(); 

@endphp

<div id="about" class="thim-block-elementor-home-01">
    <div class="container-content-wrap">
        <center>
            <h1>
                @if($questionSet->cat_id)

                   
                 {{ $parentTitle->title }}  - {{ $questionSet->title }} 
                @else
                    {{ $questionSet->title }} 
                @endif
            </h1>
        </center>
        <div class="row-content-wrap">

            <div class="col-lg-7 wow fadeInRight" style="visibility: visible; animation-name: fadeInRight">
                <div class="start-quiz row justify-content-center">
                    <button class='start-button dot'>START</button>
                </div>
                <div class="content-img questions">
                    <ul class="list row"> </ul>

                </div>
                <div class="row justify-content-end">
                    <div class='submit text-right cp mr-3'>
                        <button id="check_answer" class="submit-button btn btn-info">Check Answer</button>
                    </div>
                    <div class='next text-right cp mr-3'>
                        <button class="next-button btn btn-success">Next</button>
                    </div>
                    <div class='end-quiz'>
                        <p class='end-score'></p>
                        <p class='comment'></p>
                        <button class="retake-button">RETAKE QUIZ</button>
                    </div>
                </div>

            </div>
            <div class="col-lg-5 wow fadeInLeft" style="visibility: visible; animation-name: fadeInLeft">
                <div class="content-box">
                    <div class="start-quiz">
                        <h2 class='descrip'>Test your knowledge ?</h2>
                    </div>
                    <div class="content-text">
                        <h3 class='question-number'></h3>
                        <h2 class='question'></h2>

                    </div>
                    <div class="cp mt-xl-5 row justify-content-center">
                        <img id="question_image" src="#" width="300px" height="150px" />
                    </div>
                    <div class="content-check">
                        <div class="box col-lg-9">
                            <h4 class="score"> </h4>

                        </div>

                        <div class="box col-lg-9">
                            <button id="complete_answer" style="visibility: hidden" class="next-button btn btn-info" onclick="completeExam()">Finsh Exam</button>
                        </div>


                    </div>
                </div>
            </div>
            <input type="hidden" name="questions" id="questions" class="form-control form-control-lg" readonly value="{{ $questions }}" />
            <input type="hidden" name="question_set_id" id="question_set_id" class="form-control form-control-lg" readonly value="{{ $question_set_id }}" />
        </div>
    </div>
</div>


<!-- */*************************START PAGE**************************/* -->
<!-- <div class="container">
        <h1>Financial Quiz</h1>

        <div class="start-quiz">
            <p class='descrip'>Test your knowledge on the Finance</p>
            <button class='start-button dot'>START</button>
        </div>




        <div class='questions'>
            <h2 class='question'></h2>
            <p class="score"> Score: </p>
            <ul class="list">
            </ul>

            <div class='submit'>
                <button class="submit-button">Check Answer</button>
            </div>
            <div class='next'>
                <button class="next-button">Next</button>
            </div>

            <p class='question-number'></p>
        </div>


        <div class='end-quiz'>
            <p class='end-score'></p>
            <p class='comment'></p>
            <button class="retake-button">RETAKE QUIZ</button>
        </div>
    </div> -->
<!-- */****************************************** -->


@endsection

@section('extra-js')
<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script src="final.js"></script>
<script>
    // var jobs = JSON.parse("{{ $questions }}");

    // console.log(jobs);

    var questions = document.getElementById("questions").value;

    var question_set_id = document.getElementById("question_set_id").value;
    var questionList = JSON.parse(questions);
    const total_length = questionList.length;
    console.log(questionList.length);


    var score = 0;
    var current = 0;
    var already_submit_answer = 0;

    $(document).ready(function() {
        // Create an event listener to listen for a click on the Start Quiz button
        $(".start-button").click(function() {
            $(".start-quiz").hide();
            $(".next").hide();
            $(".questions").show();
            $(".cp").show();
            displayQuestion();
            $(".score").text("Current Score: " + score);
            // $("#complete_answer").show();
            $("#complete_answer").css('visibility', 'visible');
            console.log("Start Quiz button clicked");
        });

        // Create an event listener to listen for a click on the Next button
        $(".next-button").click(function(event) {
            console.log("Next button clicked");
            already_submit_answer = 0;
            displayQuestion();
            $(".next").hide();
            $(".submit").show();
        });

        $(".submit-button").click(function(event) {
            if ($("li.selected").length) {
                var answer = $("li.selected").attr("id");
                checkAnswer(answer);
                $(".next").show();
                $(".submit").hide();
            } else {
                alert("Please select an answer");
            }
        });

        // Create an event listener to listen for a click on the Retake button and refresh the page
        $(".retake-button").click(function() {
            location.reload();
            console.log("Retake button clicked");
        });

        //Click listener when clicking on a list item to change the color of the background
        $("ul.list").on("click", "li", function(event) {

            if (already_submit_answer == 0) {
                $(".selected").removeClass();
                $(this).addClass("selected");
            }
        });
    });

    //***************FUNCTIONS**************
    function displayQuestion() {

        var tempImageLink = "";
        if (current > (questionList.length - 1)) {
            document.getElementById("check_answer").style.display = "none";
            current = questionList.length;

            $(".question-number").text("Question Number: " + current + "/" + questionList.length);
        } else {

            $(".question-number").text("Question Number: " + (current + 1) + "/" + questionList.length);
        }
        if (current < questionList.length) {
            var listQuestion = questionList[current];

            if (listQuestion.question_text != null && listQuestion.question_text != undefined) {
                $("h2").text(listQuestion.question_text);
            }

            if (listQuestion.question_image != null && listQuestion.question_image != undefined) {
                tempImageLink = "{{ asset('../images/questions/') }}/" + listQuestion.question_image;
                $("#question_image").attr('src', tempImageLink);
                $("#question_image").show();
            } else {
                $("#question_image").hide();
            }


            tempOptionImageLink1 = "{{ asset('../images/questions/') }}/" + listQuestion.option_1_image;
            tempOptionImageLink1 = (listQuestion.option_1_image != null) ? ('<img src=" ' + tempOptionImageLink1 + '" alt="Girl in a jacket" width="200px" height="150px">') : "";
            tempOptionText1 = (listQuestion.option_1_text != null) ? listQuestion.option_1_text : "";

            tempOptionImageLink2 = "{{ asset('../images/questions/') }}/" + listQuestion.option_2_image;
            tempOptionImageLink2 = (listQuestion.option_2_image != null) ? ('<img src=" ' + tempOptionImageLink2 + '" alt="Girl in a jacket" width="200px" height="150px">') : "";
            tempOptionText2 = (listQuestion.option_2_text != null) ? listQuestion.option_2_text : "";

            tempOptionImageLink3 = "{{ asset('../images/questions/') }}/" + listQuestion.option_3_image;
            tempOptionImageLink3 = (listQuestion.option_3_image != null) ? ('<img src=" ' + tempOptionImageLink3 + '" alt="Girl in a jacket" width="200px" height="150px">') : "";
            tempOptionText3 = (listQuestion.option_3_text != null) ? listQuestion.option_3_text : "";

            tempOptionImageLink4 = "{{ asset('../images/questions/') }}/" + listQuestion.option_4_image;
            tempOptionImageLink4 = (listQuestion.option_4_image != null) ? ('<img src=" ' + tempOptionImageLink4 + '" alt="Girl in a jacket" width="200px" height="150px">') : "";
            tempOptionText4 = (listQuestion.option_4_text != null) ? listQuestion.option_4_text : "";



            $("ul.list").html("");

            $("ul.list").append(
                '<li id = "' + 1 + '">' + tempOptionText1 + tempOptionImageLink1 + "</li>" +
                '<li id = "' + 2 + '">' + tempOptionText2 + tempOptionImageLink2 + "</li>" +
                '<li id = "' + 3 + '">' + tempOptionText3 + tempOptionImageLink3 + "</li>" +
                '<li id = "' + 4 + '">' + tempOptionText4 + tempOptionImageLink4 + "</li>"
            );

            // <img src="img_girl.jpg" alt="Girl in a jacket" width="500" height="600">

            // $("ul.list").append(
            //     '<li id = "' + 1 + '">' + listQuestion.option_1_text + "</li>" +
            //     '<li id = "' + 2 + '">' + listQuestion.option_2_text + "</li>" +
            //     '<li id = "' + 3 + '">' + listQuestion.option_3_text + "</li>" +
            //     '<li id = "' + 4 + '">' + listQuestion.option_4_text + "</li>"
            // );

        } else {
            // show summary that says how many you got correct
            displayScore();
            completeExam();
            submitAnswer();

            //  Ajax call 
            // question_set_id,
            // score
        }
    }

    // Checks answer from the array to see if the one chosen is the one that is correct
    function checkAnswer(answer) {
        var listQuestion = questionList[current];
        already_submit_answer = 1;
        console.log("answer :" + answer);
        console.log("istQuestion.correct_answer :" + listQuestion.correct_answer);
        if (listQuestion.correct_answer == answer) {
            score++;
            $(".list li.selected").addClass("correct");
        } else {
            $(".list li.selected").addClass("incorrect");
            $('.list li').eq(listQuestion.correct_answer - 1).addClass('correct');
        }

        $(".score").text("Current Score: " + score);
        current++;
    }

    //Display score
    function displayScore() {
        $(".questions").hide();
        $("#question_image").hide();
        $(".cp").hide();
        $(".end-quiz").show();
        $(".end-score").text("Your score is: " + score + " / " + questionList.length);
        var percentage = (score * 100) / questionList.length;
        $("h2").text("");




        if (percentage > 70) {
            $(".comment").text("Percentage: " + parseInt(percentage) + "%, You are Passed");
            tempImageLink = "{{ asset('../images/') }}/pass.jpg";
        } else {
            $(".comment").text("Percentage: " + parseInt(percentage) + "%, Sorry you are Failed");
            tempImageLink = "{{ asset('../images/') }}/fail.png";
        }

        document.getElementById("question_image").src = tempImageLink;

        $("#question_image").show();
        $(".cp").show();
    }

    // completeExam
    function completeExam() {
        current = questionList.length + 1;
        $("#complete_answer").hide();
    }


    function submitAnswer() {

        let final_question_set_id = question_set_id;
        let final_score = score;
        let total_question = total_length;

        $.ajax({
            url: "{{route('participant.exam-submit')}}",
            type: 'POST',
            data: {
                "_token": "{{ csrf_token() }}",
                'final_score': final_score,
                'final_question_set_id': final_question_set_id,
                'total_question_data': total_question
            },

            success: function(response) {

                try {
                    response = JSON.parse(response);
                    console.log(response);
                    if (response.success == false) {
                        // alert(response.message);
                    } else {
                        // alert(response.message);
                        // console.log(response.data);
                        // console.log("saved");
                    }
                } catch (error) {

                }
            },
            error: function(xhr, textStatus, errorThrown) {

                // console.log("Fail");
                // console.log(textStatus);
                // console.log(errorThrown);
                // console.log(xhr);
                // alert("Fail");
            }
        });
    }
</script>
@endsection