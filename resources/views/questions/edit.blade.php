@extends('layouts.app_admin')
@section('title', 'Question')
@push('css')


@endpush

@section('content')

<!-- Main Content -->
<div class="main-content">
    <section class="section">
        <div class="col-12 section-header">
            <div class="col-6">
                <h1> Edit Question</h1>
            </div>

        </div>
        @if ($errors->any())
        <div class="col-sm-12">
            <div class="alert  alert-warning alert-dismissible fade show" role="alert">
                @foreach ($errors->all() as $error)
                <span>
                    <p>{{ $error }}</p>
                </span>
                @endforeach
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        </div>
        @endif

        @include('flash-message')

        <div class="section-body">
            <div class="col-12">
                <div class="card">
                    {{-- {{ route('product.store') }} --}}
                    <div class="col-12 card-body">

                        <form method="POST" action="{{ route('questions.update', $id) }}" id="editForm" enctype="multipart/form-data">
                            {{ method_field('PATCH') }}
                            {{ csrf_field() }}
                            <div class="row">
                                <div class="col-lg-5">
                                    <!-- <div class="row"> -->
                                    <div class="form-group">
                                        <label>Question (Text)</label><br>
                                        <!-- <input type="text" name="question_text" class="form-control form-control-lg" value="{{ $question->question_text }}" /> -->
                                        <textarea name="question_text" class="form-control form-control-lg">{{ $question->question_text }}</textarea>
                                    </div>

                                    <div class="form-group">
                                        <label>Question (Image)</label><br>
                                        @if ($question->question_image != null)
                                        <img src="{{ asset('images/questions/' . $question->question_image) }}" width="300px" height="200px" alt="user photo" /><br>


                                        @endif
                                        <input type="file" name="question_image" class="form-control form-control-lg" />
                                    </div>
                                    <!-- </div> -->
                                </div>

                                <div class="col-lg-7">
                                    <div class="row">
                                        <div class="col-6">
                                            <div class="form-group">
                                                <label>Option 1 (Text)</label>
                                                <textarea name="option_1_text" class="form-control form-control-lg ctHeight"> {{ $question->option_1_text }}</textarea>
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="form-group">
                                                <label>Option 1 (Image)</label>
                                                @if ($question->option_1_image != null)
                                                <img src="{{ asset('images/questions/' . $question->option_1_image) }}" width="60" height="60" alt="user photo" style="border-radius: 50%;padding-left: 0px;" /><br>

                                                @endif
                                                <input type="file" name="option_1_image" class="form-control form-control-lg" />

                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-6">
                                            <div class="form-group">
                                                <label>Option 2 (Text)</label>
                                                <textarea name="option_2_text" class="form-control form-control-lg ctHeight">{{ $question->option_2_text }}</textarea>
                                            </div>
                                        </div>
                                        <div class="col-6">

                                            <div class="form-group">
                                                <label>Option 2 (Image)</label>
                                                @if ($question->option_2_image != null)
                                                <img src="{{ asset('images/questions/' . $question->option_2_image) }}" width="60" height="60" alt="user photo" style="border-radius: 50%;padding-left: 0px;" /><br>

                                                @endif
                                                <input type="file" name="option_2_image" class="form-control form-control-lg" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-6">
                                            <div class="form-group">
                                                <label>Option 3 (Text)</label>
                                                <textarea name="option_3_text" class="form-control form-control-lg ctHeight">{{ $question->option_3_text }}</textarea>
                                            </div>
                                        </div>
                                        <div class="col-6">

                                            <div class="form-group">
                                                <label>Option 3 (Image)</label>
                                                @if ($question->option_3_image != null)
                                                <img src="{{ asset('images/questions/' . $question->option_3_image) }}" width="60" height="60" alt="user photo" style="border-radius: 50%;padding-left: 0px;" /><br>

                                                @endif
                                                <input type="file" name="option_3_image" class="form-control form-control-lg" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-6">
                                            <div class="form-group">
                                                <label>Option 4 (Text)</label>
                                                <textarea name="option_4_text" class="form-control form-control-lg ctHeight">{{ $question->option_4_text }}</textarea>
                                            </div>
                                        </div>
                                        <div class="col-6">

                                            <div class="form-group">
                                                <label>Option 4 (Image)</label>
                                                @if ($question->option_4_image != null)
                                                <img src="{{ asset('images/questions/' . $question->option_4_image) }}" width="60" height="60" alt="user photo" style="border-radius: 50%;padding-left: 0px;" /><br>

                                                @endif
                                                <input type="file" name="option_4_image" class="form-control form-control-lg" />
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label>Correct Answer</label>
                                        <input type="number" name="correct_answer" class="form-control form-control-lg" value="{{ $question->correct_answer }}" />
                                    </div>
                                </div>
                            </div>


                            <input type="hidden" name="id" id="id" class="form-control form-control-lg" readonly value="{{ $id }}" />

                            <input type="hidden" name="question_set_id" id="question_set_id" class="form-control form-control-lg" readonly value="{{ $question->question_set_id }}" />

                            <div class="card-footer text-right">
                                <button class="btn btn-primary mr-1" type="submit">Update</button>

                            </div>

                    </div>
                </div>
                </form>
            </div>
        </div>
</div>
</section>


<!-- End -->
</div>



@endsection

@section('extra-js')


@endsection