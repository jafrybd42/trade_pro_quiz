@extends('layouts.app_admin')
@section('title', 'Question')
    @push('css')


    @endpush

@section('content')

    <!-- Main Content -->
    <div class="main-content">
        <section class="section">
            <div class="col-12 section-header">
                <div class="col-6">
                    <h1> Upload Question</h1>
                </div>

            </div>
            @if ($errors->any())
            <div class="col-sm-12">
                <div class="alert  alert-warning alert-dismissible fade show" role="alert">
                    @foreach ($errors->all() as $error)
                        <span>
                            <p>{{ $error }}</p>
                        </span>
                    @endforeach
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </div>
        @endif

        @include('flash-message')

            <div class="section-body">
                <div class="col-6">
                    <div class="card">
                        {{--  --}}
                        <div class="card-body">
                            <form method="POST" action="{{ route('questions.importQuestions') }}" id="editForm" enctype="multipart/form-data">
                                {{ method_field('POST') }}
                                {{ csrf_field() }}

                                <div class="form-group">
                                    <label>Upload File ( Excel Format)<code></code></label>
                                    <input type="file" name="upload_question" class="form-control form-control-lg" />
                                </div>
                               
                                <input type="hidden" name="question_set_id" id="question_set_id" class="form-control form-control-lg" readonly value="{{ $question_set_id }}"/>
                                <div class="card-footer text-right">
                                    <button class="btn btn-primary mr-1" type="submit">Submit</button>

                                </div>

                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>


        <!-- End -->
    </div>



@endsection

@section('extra-js')

@endsection

